/**
 * This file is part of gnome-sound-record.
 * Copyright (C) Martin Blanchard 2013 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <gst/gst.h>

#include "gsr-recorder.h"

enum {
  PROP_0,
  PROP_LAST_RECORD_TIMEVAL,
  PROP_RECORDS_PATH
};

enum {
  RECORDING_STARTED,
  RECORDING_STOPPED,
  PIPELINE_ERROR,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

struct _GsrRecorderPrivate
{
  gchar *path;
  gchar *id;

  GstBus *bus;

  GstElement *source;
  GstElement *converter;
  GstElement *encoder;
  GstElement *sink;

  GDateTime *last_record_timeval;
  guint record_count;
};

static const gchar *id_characters = "0123456789abcdef";

static gchar *days[] = {
  NULL,
  N_("Mon."),
  N_("Tue."),
  N_("Wen."),
  N_("Thu."),
  N_("Fri."),
  N_("Sat."),
  N_("Sun.")
};

static gchar *months[] = {
  NULL,
  N_("January"),
  N_("February"),
  N_("March"),
  N_("April"),
  N_("May"),
  N_("June"),
  N_("July"),
  N_("August"),
  N_("September"),
  N_("October"),
  N_("November"),
  N_("December")
};

#define GSR_RECORDER_ID_SIZE              32
#define GSR_RECORDER_ID_CHARACTERS_SIZE   16

#define GSR_RECORDER_GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GSR_TYPE_RECORDER, GsrRecorderPrivate))

G_DEFINE_TYPE (GsrRecorder, gsr_recorder, GST_TYPE_PIPELINE)

static void    gsr_recorder_set_file            (GsrRecorder *recorder);
static void    gsr_recorder_write_title_tag     (GsrRecorder *recorder, GFile *file);

static void    gsr_recorder_error_cb            (GstBus *bus, GstMessage *message, gpointer data);
static void    gsr_recorder_element_cb          (GstBus *bus, GstMessage *message, gpointer data);
static void    gsr_recorder_state_changed_cb    (GstBus *bus, GstMessage *message, gpointer data);
static void    gsr_recorder_eos_cb              (GstBus *bus, GstMessage *message, gpointer data);

static void
gsr_recorder_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;
  gint64 timeval = 0;

  recorder = (GsrRecorder *) object;
  priv = recorder->priv;

  switch (prop_id) {
    case PROP_LAST_RECORD_TIMEVAL: {
      timeval = (gint64) g_value_get_uint64 (value);
      priv->last_record_timeval = g_date_time_new_from_unix_local (timeval);

      break;
    } case PROP_RECORDS_PATH: {
      priv->path = g_strdup (g_value_get_string (value));

      break;
    } default: {
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
  }
}

static void
gsr_recorder_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;

  recorder = (GsrRecorder *) object;
  priv = recorder->priv;

  switch (prop_id) {
    case PROP_RECORDS_PATH: {
      g_value_set_string (value, g_strdup (priv->path));

      break;
    } default: {
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
  }
}

static void
gsr_recorder_dispose (GObject *object)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;

  recorder = (GsrRecorder *) object;
  priv = recorder->priv;

  if (priv->bus != NULL) {
    gst_object_unref (GST_OBJECT (priv->bus));
  }
  priv->bus = NULL;

  if (priv->last_record_timeval != NULL) {
    g_date_time_unref (priv->last_record_timeval);
  }
  priv->last_record_timeval = NULL;

  G_OBJECT_CLASS (gsr_recorder_parent_class)->dispose (object);
}

static void
gsr_recorder_finalize (GObject *object)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;

  recorder = (GsrRecorder *) object;
  priv = recorder->priv;

  g_free (priv->path);
  if (priv->id != NULL) {
    g_free (priv->id);
  }
  priv->path = NULL;
  priv->id = NULL;

  G_OBJECT_CLASS (gsr_recorder_parent_class)->finalize (object);
}

static void
gsr_recorder_class_init (GsrRecorderClass *klass)
{
  GObjectClass *gobject_class = NULL;

  gobject_class = (GObjectClass *) klass;
  gobject_class->set_property = gsr_recorder_set_property;
  gobject_class->get_property = gsr_recorder_get_property;
  gobject_class->dispose = gsr_recorder_dispose;
  gobject_class->finalize = gsr_recorder_finalize;

  g_object_class_install_property (gobject_class,
                                   PROP_LAST_RECORD_TIMEVAL,
                                   g_param_spec_uint64 ("last-record-timeval",
                                   "Last record timeval",
                                   "The timeval of last record (should be use only once at creation time).",
                                   0, G_MAXUINT64, 0,
                                   G_PARAM_WRITABLE));

  g_object_class_install_property (gobject_class,
                                   PROP_RECORDS_PATH,
                                   g_param_spec_string ("record-path",
                                   "Records directory",
                                   "A path to the directory where records are put",
                                   NULL,
                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

  signals[RECORDING_STARTED] = g_signal_new ("record-started",
                                             G_TYPE_FROM_CLASS (gobject_class),
                                             G_SIGNAL_RUN_LAST,
                                             G_STRUCT_OFFSET (GsrRecorderClass, record_started),
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__VOID,
                                             G_TYPE_NONE, 0);

  signals[RECORDING_STOPPED] = g_signal_new ("record-stopped",
                                             G_TYPE_FROM_CLASS (gobject_class),
                                             G_SIGNAL_RUN_LAST,
                                             G_STRUCT_OFFSET (GsrRecorderClass, record_stopped),
                                             NULL, NULL,
                                             g_cclosure_marshal_VOID__STRING,
                                             G_TYPE_NONE, 1, G_TYPE_STRING);

  /*signals[PIPELINE_ERROR] = g_signal_new ("pipeline-error",
                                          G_TYPE_FROM_CLASS (gobject_class),
                                          G_SIGNAL_RUN_LAST,
                                          G_STRUCT_OFFSET (GsrRecorderClass, record_stopped),
                                          NULL, NULL,
                                          g_cclosure_marshal_VOID__POINTER,
                                          1, GSR_ERROR, G_TYPE_NONE, 0);*/

  g_type_class_add_private (gobject_class, sizeof (GsrRecorderPrivate));
}

static void
gsr_recorder_init (GsrRecorder *recorder)
{
  GsrRecorderPrivate *priv = NULL;

  priv = GSR_RECORDER_GET_PRIVATE (recorder);
  recorder->priv = priv;

  gst_object_set_name (GST_OBJECT(recorder), "GsrRecorder");

  priv->path = NULL;
  priv->id = NULL;

  priv->bus = NULL;

  priv->source = NULL;
  priv->converter = NULL;
  priv->encoder = NULL;
  priv->sink = NULL;

  priv->last_record_timeval = NULL;
  priv->record_count = 1;
}

GsrRecorder*
gsr_recorder_new (const gchar *directory)
{
  g_return_val_if_fail ((directory != NULL), NULL);

  return g_object_new (GSR_TYPE_RECORDER, "record-path", directory, NULL);
}

void
gsr_recorder_start_recording (GsrRecorder *recorder, GError **error)
{
  GsrRecorderPrivate *priv = NULL;

  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  if (G_UNLIKELY (priv->id != NULL)) {
    /* We are alredy recording ... */
    return;
  }

  if (G_UNLIKELY (priv->bus == NULL)) {
    priv->source = gst_element_factory_make ("pulsesrc", "GsrRecorderSource");
    priv->converter = gst_element_factory_make ("audioconvert", "GsrRecorderConverter");
    priv->encoder = gst_element_factory_make ("flacenc", "GsrRecorderEncoder");
    priv->sink = gst_element_factory_make ("giosink", "GsrRecorderSink");

    if (priv->source == NULL || priv->converter == NULL
        || priv->encoder == NULL || priv->sink == NULL) {
/*        *error = g_error_new (GSR_ERROR, GSR_ERROR_GST_MISSING_PLUGINS,*/
/*                              _("You need the \"good\" set of gstreamer's plugin."));*/

      return;
    }

    gst_bin_add_many (GST_BIN (recorder),
                      priv->source,
                      priv->converter,
                      priv->encoder,
                      priv->sink,
                      NULL);

    gst_element_link_many (priv->source,
                           priv->converter,
                           priv->encoder,
                           priv->sink,
                           NULL);

    priv->bus = gst_pipeline_get_bus (GST_PIPELINE (recorder));
    gst_bus_add_signal_watch (priv->bus);
    g_signal_connect (priv->bus, "message::error",
                      G_CALLBACK (gsr_recorder_error_cb), recorder);
    g_signal_connect (priv->bus, "message::element",
                      G_CALLBACK (gsr_recorder_element_cb), recorder);
    g_signal_connect (priv->bus, "message::state-changed",
                      G_CALLBACK (gsr_recorder_state_changed_cb), recorder);
    g_signal_connect (priv->bus, "message::eos",
                      G_CALLBACK (gsr_recorder_eos_cb), recorder);
  }

  gsr_recorder_set_file (recorder);
  gst_element_set_state (GST_ELEMENT (recorder), GST_STATE_PLAYING);
}

void
gsr_recorder_stop_recording (GsrRecorder *recorder)
{
  GsrRecorderPrivate *priv = NULL;
  GstEvent *event = NULL;

  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  if (G_UNLIKELY (priv->id == NULL)) {
    /* We are not recording... */
    return;
  }

  event = gst_event_new_eos ();
  if (event != NULL) {
     gst_element_send_event (GST_ELEMENT(recorder), event);

    gst_element_set_state (priv->source, GST_STATE_NULL); /* Will emit EOS */
    gst_element_get_state (priv->source, NULL, NULL, GST_CLOCK_TIME_NONE);
    gst_element_set_locked_state (priv->source, TRUE);
  }
}

static void
gsr_recorder_set_file (GsrRecorder *recorder)
{
  GsrRecorderPrivate *priv = NULL;
  gchar *id = NULL;
  gchar *file, *uri;
  gint i = 0;

  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  if (priv->id != NULL) {
    /* We are alredy recording ... */
    return;
  }

  id = g_malloc (sizeof (gchar) * (GSR_RECORDER_ID_SIZE + 1));
  id[GSR_RECORDER_ID_SIZE] = 0;

  while (i < GSR_RECORDER_ID_SIZE) {
    gdouble random;
    guint character;

    random = g_random_double ();
    character = random * (gdouble) (GSR_RECORDER_ID_CHARACTERS_SIZE - 1);
    id[i] = id_characters[character];
    ++i;
  }

  if (id == NULL) {
    return;
  }

  file = g_build_filename (priv->path, g_strconcat (id, ".flac", NULL), NULL);

  priv->id = g_strdup (id);
  uri = g_filename_to_uri (file, NULL, NULL);
  g_object_set (G_OBJECT (priv->sink), "location", uri, NULL);

  g_free (uri);
  g_free (file);
  g_free (id);
}

static void
gsr_recorder_write_title_tag (GsrRecorder *recorder, GFile *file)
{
  GsrRecorderPrivate *priv = NULL;
  GError *error = NULL;
  GFileInfo *info;
  GDateTime *time = NULL;
  GTimeSpan difference = 0;
  gchar *title = NULL;
  gchar *suffix = NULL;
  gint64 date;
  gint weekday, day, month, hour, minute;

  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  if (file == NULL) {
    title = _("Unknow record");
  } else {
    info = g_file_query_info (file,
                              G_FILE_ATTRIBUTE_TIME_MODIFIED,
                              G_FILE_QUERY_INFO_NONE,
                              NULL,
                              &error);

    if (error != NULL) {
      g_warning (error->message);
      title = _("Unknow record");
    } else {
      date = (gint64) g_file_info_get_attribute_uint64 (info, G_FILE_ATTRIBUTE_TIME_MODIFIED);
      time = g_date_time_new_from_unix_local (date);

      g_object_unref (G_OBJECT (info));

      weekday = g_date_time_get_day_of_week (time);
      day = g_date_time_get_day_of_month (time);
      month = g_date_time_get_month (time);
      hour = g_date_time_get_hour (time);
      minute = g_date_time_get_minute (time);

      if (priv->last_record_timeval != NULL) {
        difference = g_date_time_difference (time, priv->last_record_timeval);
        difference /= G_TIME_SPAN_SECOND;

        if (difference < 60) {
          if (difference < (60 - g_date_time_get_seconds (priv->last_record_timeval))) {
          priv->record_count++;
          suffix = g_strconcat (" [",
                                g_strdup_printf ("%u", priv->record_count),
                                "]",
                                NULL);
          } else {
            priv->record_count = 1;
          }
        }

        g_date_time_unref (priv->last_record_timeval);
      }
      priv->last_record_timeval = time;

      title = g_strconcat (days[weekday],
                           g_strdup_printf (" %d ", day),
                           months[month],
                           g_strdup_printf (" %02d", hour),
                           g_strdup_printf (":%02d", minute),
                           suffix, /* May be NULL */
                           NULL);
    }
  }

  if (g_utf8_validate (title, -1, NULL)) {
    gst_tag_setter_add_tags (GST_TAG_SETTER (priv->encoder),
                             GST_TAG_MERGE_REPLACE,
                             GST_TAG_TITLE, title, /* Duplicate title ? */
                             NULL);
  }

  if (suffix != NULL) {
    g_free (suffix);
  }
  g_free (title);
}

static void
gsr_recorder_error_cb (GstBus *bus, GstMessage *message, gpointer data)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;
  GError *error;

  recorder = (GsrRecorder *) data;
  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

/*  error = g_error_new (GSR_ERROR, GSR_ERROR_GST_INTERNAL,*/
/*                       _("Audio engine internal error"));*/
}

static void
gsr_recorder_element_cb (GstBus *bus, GstMessage *message, gpointer data)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;
  const GstStructure *structure;
  const GValue *magnitudes;
  const GValue *magnitude;
  gint i;

  recorder = (GsrRecorder *) data;
  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  if (message->src != GST_OBJECT (priv->sink)) {
    return;
  }

  structure = gst_message_get_structure (message);

  if (G_UNLIKELY (gst_structure_has_name (structure, "file-exists"))) {
    /* File alredy exists, shoud not happened... */
    gst_bus_set_flushing (priv->bus, TRUE);
    g_free (priv->id);

    gsr_recorder_set_file (recorder);

    gst_bus_set_flushing (priv->bus, FALSE);
    gst_element_set_state (GST_ELEMENT (recorder), GST_STATE_PLAYING);

    return;
  }
}

static void
gsr_recorder_state_changed_cb (GstBus *bus, GstMessage *message, gpointer data)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;
  GstState state;
  GFile *file = NULL;

  recorder = (GsrRecorder *) data;
  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  gst_message_parse_state_changed (message, NULL, &state, NULL);
  if (message->src != GST_OBJECT (recorder)) {
    return;
  }

  switch (state) {
    case GST_STATE_NULL: {
      break;
    } case GST_STATE_READY: {
      g_object_get (G_OBJECT (priv->sink), "file", &file, NULL);
      gsr_recorder_write_title_tag (recorder, file);
      break;
    } case GST_STATE_PLAYING: {
      g_signal_emit (recorder, signals[RECORDING_STARTED], 0);
      break;
    } default: {
      break;
    }
  }
}

static void
gsr_recorder_eos_cb (GstBus *bus, GstMessage *message, gpointer data)
{
  GsrRecorder *recorder = NULL;
  GsrRecorderPrivate *priv = NULL;
  gchar *uri = NULL;

  recorder = (GsrRecorder *) data;
  g_return_if_fail (GSR_IS_RECORDER (recorder));
  priv = recorder->priv;

  gst_element_set_state (GST_ELEMENT (recorder), GST_STATE_NULL);
  gst_element_get_state (GST_ELEMENT (recorder), NULL, NULL, GST_CLOCK_TIME_NONE);

  g_object_get (G_OBJECT (priv->sink), "location", &uri, NULL);

  g_signal_emit (recorder, signals[RECORDING_STOPPED], 0, uri);

  g_free (priv->id);
  priv->id = NULL;

  gst_element_set_locked_state (priv->source, FALSE);
}

