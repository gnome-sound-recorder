/**
 * This file is part of gnome-sound-record.
 * Copyright (C) Martin Blanchard 2013 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#ifndef __GSR_RECORDER_H__
#define __GSR_RECORDER_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define GSR_TYPE_RECORDER            (gsr_recorder_get_type ())
#define GSR_RECORDER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSR_TYPE_RECORDER, GsrRecorder))
#define GSR_RECORDER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GSR_TYPE_RECORDER, GsrRecorderClass))
#define GSR_IS_RECORDER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GSR_TYPE_RECORDER))
#define GSR_IS_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GSR_TYPE_RECORDER))
#define GSR_RECORDER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GSR_TYPE_RECORDER, GsrRecorderClass))

typedef struct _GsrRecorder          GsrRecorder;
typedef struct _GsrRecorderClass     GsrRecorderClass;
typedef struct _GsrRecorderPrivate   GsrRecorderPrivate;

struct _GsrRecorder
{
  GstPipeline parent;

  /* < private > */
  GsrRecorderPrivate *priv;
};

struct _GsrRecorderClass
{
  GstPipelineClass parent_class;

  /* < signals > */
  void (*record_started) (GsrRecorder *recorder);
  void (*record_stopped) (GsrRecorder *recorder, gchar *uri);

  void (*pipeline_error) (GsrRecorder *recorder, GError *error);
};

GType          gsr_recorder_get_type          (void) G_GNUC_CONST;
GsrRecorder*   gsr_recorder_new               (const gchar *directory);

void           gsr_recorder_start_recording   (GsrRecorder *recorder, GError **error);
void           gsr_recorder_stop_recording    (GsrRecorder *recorder);

G_END_DECLS

#endif /* __GSR_RECORDER_H__ */

