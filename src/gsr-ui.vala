/**
 * This file is part of gnome-sound-record.
 * Copyright (C) Martin Blanchard 2013 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Notify;

namespace Gsr {
  public class Ui : Window {
    private State last_state;
    private bool was_recording;
    private bool was_playing;

    private ToolButton record_button;
    private ToolButton stop_button;
    private TreeView tree_view;

    public Ui (Builder builder) {
      Box main_box = null;

      last_state = State.UNKNOWN;
      was_recording = true;
      was_playing = true;

      Window.set_default_icon_name (Config.PACKAGE);
      this.set_title (Config.PACKAGE_NAME);
      this.set_resizable (true);

      if (builder != null) {
        main_box = builder.get_object ("main-box") as Box;
        record_button = builder.get_object ("record-toolbutton") as ToolButton;
        stop_button = builder.get_object ("stop-toolbutton") as ToolButton;

        tree_view = builder.get_object ("record-treeview") as TreeView;
      }

      /* Deal with the tree view setup. */
      setup_tree_view ();

      this.add (main_box as Widget);
      builder.connect_signals (this);
      Timeout.add (60, on_timeout_cb);
      this.show_all ();

      /* HACK (temporary) */
      Global.state = State.READY;
    }

    [CCode (instance_pos = -1)]
    public void on_about_action_cb () {
      show_about_dialog (this as Window,
                         "logo-icon-name", Config.PACKAGE,
                         "title", _("About ") + Config.PACKAGE_NAME,
                         "version", Config.PACKAGE_VERSION,
                         "comments", _("A simple sound recorder for the GNOME desktop"),
                         "copyright", Global.copyrights,
                         "authors", Global.authors);
    }
    
    [CCode (instance_pos = -1)]
    public void on_record_cb () {
      if (unlikely (Global.state != State.READY))
        return;

      if (Global.now_recording == false) {
        Global.recorder.start_recording ();
      }
    }

    [CCode (instance_pos = -1)]
    public void on_stop_cb () {
      if (unlikely (Global.state != State.READY))
        return;

      if (Global.now_recording == true) {
        Global.recorder.stop_recording ();
      }
    }

    public bool on_timeout_cb () {
      if (unlikely (Global.state == State.UNKNOWN))
        return true;

      /* Detect application state changes. */
      if (unlikely (Global.state != last_state)) {
        last_state = Global.state;

        switch (Global.state) {
          case State.BUSY:
            record_button.set_sensitive (false);
            stop_button.set_sensitive (false);
            break;

          case State.READY:
            record_button.set_sensitive (true);
            stop_button.set_sensitive (true);
            break;

          default:
            break;
        }
      }

      if (Global.state == State.BUSY)
        return true;

        /* Detect recording state changes. */
        if (Global.now_recording != was_recording) {
          was_recording = Global.now_recording;

          if (Global.now_recording == true) {
            record_button.hide ();
            stop_button.show ();
          } else {
            record_button.show ();
            stop_button.hide ();
          }
        }

      return true;
    }
    
    private void setup_tree_view () {
      TreeViewColumn column = null;
      CellRenderer renderer = null;

      if (tree_view == null)
      return;

      column = new TreeViewColumn ();
      renderer = new CellRendererText ();
      if (column == null || renderer == null)
        return;

      column.set_title (_("Records"));
      column.set_expand (true);
      column.set_resizable (false);
      column.pack_start (renderer, true);

      tree_view.append_column (column);
    }
  }
}

