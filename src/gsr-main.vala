/**
 * This file is part of gnome-sound-record.
 * Copyright (C) Martin Blanchard 2013 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Gst;
using Gsr;

public enum State {
  UNKNOWN,
  BUSY,
  READY
}

namespace Global {
  /* Application state: */
  public State state;
  public bool now_recording;

  /* Usefull directories and files: */
  public string ui_file;
  public string data_dir;

  /* Core lib objects: */
  public Recorder recorder;
  
  /* Data store for records: */
  public ListStore records;
}

namespace Gsr {
  private enum Attributes {
    URI,
    TIMEVAL,
    TITLE,
    CODEC,
    SIZE,
    DURATION,
    CHANNELS,
    SAMPLE_RATE,
    BIT_RATE,
    NB
  }

  public class Main : Gtk.Application {
    private Cancellable guardian;
    private Ui ui;

    public Main (string id, ApplicationFlags flags) {
      GLib.Object (application_id: id, flags: flags);

      Global.state = State.UNKNOWN;
      Global.now_recording = false;
      Global.recorder = null;
      Global.records = null;

      ui = null;
    }

    public void on_record_started_cb () {
      if (Global.state == State.READY) {
        Global.now_recording = true;
      }
    }

    public void on_record_stopped_cb (string uri) {
      if (Global.state == State.READY) {
        Global.now_recording = false;
      }
    }

    public void on_activate_cb () {
      Recorder recorder = null;
      Builder builder = null;

      if (this.get_windows () != null) {
        /* An other instance alredy exists, just show it */
        ui.present ();
      } else {
        /* Increase the application ref-counter. */
        this.hold ();

        Global.state = State.BUSY;
        Environment.set_application_name (Config.PACKAGE_NAME);
        Environment.set_variable ("PULSE_PROP_media.role", "production", true);

        recorder = new Recorder (Global.data_dir);
        if (recorder == null) {
          warning (_("Impossible to allocate core recorder object..."));
          return;
        }

        /* Those signals inform us of the recording engine state. */
        recorder.record_started.connect (on_record_started_cb);
        recorder.record_stopped.connect (on_record_stopped_cb);
        Global.recorder = recorder;

        builder = new Builder ();
        if (builder == null) {
          warning (_("Impossible to allocate a new GtkBuilder object..."));
          return;
        }
        try {
          builder.add_from_file (Global.ui_file);
        } catch (Error error) {
          warning (error.message);
          return;
        }

        /* The record data store is declared in the .ui file. */
        Global.records = builder.get_object ("main-liststore") as ListStore;
        if (Global.records == null) {
          warning (_("Impossible to allocate a new store for records object..."));
          return;
        }

        ui = new Ui (builder);
        if (ui == null) {
          warning (_("Failed at building the GTK+ interface..."));
          return;
        }

        /* Look for previous records. */
        analyze_data_directory.begin ();

        this.add_window (ui);
        ui.destroy.connect (this.on_destroy_cb);
        ui.set_application (this);
      }
    }
    
    public void on_destroy_cb () {
      just_quit ();
    }

    private void just_quit () {
      switch (Global.state) {
        case State.BUSY:
          if (guardian != null)
            guardian.cancel ();
          break;
        case State.READY:
          break;
        default:
          break;
      }

      /* Decreasing the ref-counter should be enought ! */
      this.release ();
    }

    private async void analyze_data_directory () {
      FileEnumerator enumeration = null;
      File directory = null;
      TreeIter iter;

      guardian = new Cancellable ();
      directory = File.new_for_path (Global.data_dir);
      try {
        enumeration = yield directory.enumerate_children_async (
                       FileAttribute.STANDARD_NAME + "," + FileAttribute.TIME_MODIFIED,
                       FileQueryInfoFlags.NONE, Priority.DEFAULT, guardian);

        while (true) {
          var files = yield enumeration.next_files_async (10, Priority.DEFAULT);
          if (files == null) {
            foreach (var info in files) {
              if (info.get_name ().has_suffix (".flac") == true) {
                uint64 timeval = 0;
                string filename = null;
                string uri = null;

                filename = Path.build_filename (Global.data_dir, info.get_name ());
                uri = Filename.to_uri (filename, null);
                timeval = info.get_attribute_uint64 (FileAttribute.TIME_MODIFIED);

                if (uri != null)
                  add_record (uri, timeval);
              }
            }
          } else {
            uint64 timeval = 0;
            /* No more files, give last record creation time to the Recorder for naming stuff: */
            if (Global.records.get_iter_first (out iter) == true) {
              Global.records.get (iter, Attributes.TIMEVAL, out timeval);
              if (timeval != 0)
                Global.recorder.set ("last-record-timeval", timeval);
            }

            Global.state = State.READY;
            break;
          }
        }
      } catch (Error error) {
        /* Something went wrong while parsing the data directory... */
        warning (error.message);
      }
    }

    private TreeIter add_record (string uri, uint64 timeval = uint64.MAX) {
      TreeIter iter;

      Global.records.prepend (out iter);
      Global.records.set (iter, Attributes.URI, uri, Attributes.TIMEVAL, timeval);

      return iter;
    }
  }

  public int main (string[] args) {
    File directory = null;
    Main app = null;
    int status = 1;

    Gtk.init (ref args);

    Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.PACKAGE_LOCALEDIR);
    Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Config.GETTEXT_PACKAGE);

    //Global.ui_file = Path.build_filename (Config.PACKAGE_DATADIR, Config.PACKAGE, "main.ui");
    Global.ui_file = Path.build_filename (".", "data", "main.ui");
    Global.data_dir = Path.build_filename (Environment.get_user_data_dir (), Config.PACKAGE);

    directory = File.new_for_path (Global.data_dir);
    if (directory.query_exists () == false) {
      try {
        /* Our xdg data dir doesn't exists, create it. */
        directory.make_directory ();
      } catch (Error error) {
        warning (error.message);
        return 1;
      }
    }

    Gst.init (ref args);
    Notify.init (Config.PACKAGE_NAME);

    app = new Main ("org.gnome." + Config.PACKAGE_SAFENAME, ApplicationFlags.FLAGS_NONE);
    app.activate.connect (app.on_activate_cb);

    status = app.run (args);

    Notify.uninit ();

    return status;
  }
}

