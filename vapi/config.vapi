/**
 * This file is part of gnome-sound-recorder.
 * Copyright (C) Martin Blanchard 2012 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

[CCode (prefix = "",
        lower_case_cprefix = "",
        cheader_filename = "config.h")]
namespace Config {
  public const string PACKAGE;
  public const string PACKAGE_NAME;
  public const string PACKAGE_SAFENAME;
  public const string PACKAGE_VERSION;
  public const string PACKAGE_URL;
  public const string PACKAGE_DATADIR;
  public const string PACKAGE_LOCALEDIR;
  public const string GETTEXT_PACKAGE;
}

