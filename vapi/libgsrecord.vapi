/**
 * This file is part of gnome-sound-record.
 * Copyright (C) Martin Blanchard 2013 <tinram@gmx.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

using GLib;
using Gtk;
using Gst;


[CCode (prefix = "Gsr",
        lower_case_cprefix = "gsr_",
        cheader_filename = "gsr-recorder.h")]
namespace Gsr {
  [Compact]
  [CCode (free_function = "gst_object_unref")]
  public class Recorder : Gst.Pipeline {
    [CCode (cname = "gsr_recorder_new")]
    public Recorder (string directory);

    [CCode (cname = "gsr_recorder_start_recording")]
    public void start_recording (Error *error = null);
    [CCode (cname = "gsr_recorder_stop_recording")]
    public void stop_recording ();

    public signal void record_started ();
    public signal void record_stopped (string uri);
  }
}

